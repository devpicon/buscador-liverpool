package pe.devpicon.android.buscadorliverpool.data.client;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import pe.devpicon.android.buscadorliverpool.data.db.DatabaseManager;
import pe.devpicon.android.buscadorliverpool.mvp.model.Product;
import pe.devpicon.android.buscadorliverpool.mvp.presenter.PresenterOps;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by armando on 4/11/17.
 */

public class LiverpoolRepository {

    private static final String TAG = LiverpoolRepository.class.getSimpleName();

    public void getProducts(String criteria, final PresenterOps presenterOps) {
        LiverpoolService service = LiverpoolService.retrofit.create(LiverpoolService.class);
        Call<JsonObject> call = service.getProductsJsonObject(criteria);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject body = response.body();

                    List<Product> products = new ArrayList<Product>();

                    JsonArray contents = body.getAsJsonArray("contents");
                    if (!contents.isJsonNull()) {
                        JsonObject jsonElement = contents.get(0).getAsJsonObject();
                        JsonArray mainContent = jsonElement.getAsJsonArray("mainContent");

                        JsonObject contentsIn = mainContent.get(3).getAsJsonObject();
                        JsonArray contents1 = contentsIn.getAsJsonArray("contents");

                        JsonObject asJsonObject = contents1.get(0).getAsJsonObject();

                        JsonArray records = asJsonObject.getAsJsonArray("records");

                        for (int i = 0; i < records.size(); i++) {

                            JsonObject record = records.get(i).getAsJsonObject();

                            Product product = new Product(
                                    record.getAsJsonObject("attributes").get("product.displayName")
                                            .getAsString(),
                                    record.getAsJsonObject("attributes").get("sku.sale_Price")
                                            .getAsString(),
                                    record.getAsJsonObject("attributes").get("product.smallImage")
                                            .getAsString()
                            );

                            products.add(product);

                        }

                        Log.d(TAG, records.toString());
                    }

                    presenterOps.onLoadSuccess(products);

                } else {

                    Log.d(TAG, "Ocurrió un problema: " + response.message());
                    presenterOps.onFailure();


                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                presenterOps.onFailure();
            }
        });

    }

    public void saveCriteria(Context context, String criteria) {

        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
        databaseManager.insertCriteria(criteria);

    }
}
