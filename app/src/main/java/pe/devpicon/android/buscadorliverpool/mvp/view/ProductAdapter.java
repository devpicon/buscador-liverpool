package pe.devpicon.android.buscadorliverpool.mvp.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import pe.devpicon.android.buscadorliverpool.R;
import pe.devpicon.android.buscadorliverpool.mvp.model.Product;

/**
 * Created by armando on 4/11/17.
 */

class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    List<Product> productList = new ArrayList<>();

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent,
                false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        holder.bindProduct(productList.get(position));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        View itemView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
        }

        public void bindProduct(Product product) {
            TextView txtProductName = (TextView) itemView.findViewById(R.id.txt_product_name);
            TextView txtProductPrice = (TextView) itemView.findViewById(R.id.txt_product_price);
            ImageView imgProductThumbnail = (ImageView) itemView.findViewById(R.id
                    .img_product_thumbnail);


            txtProductName.setText(product.getTitle());
            txtProductPrice.setText(product.getPrice());
            Glide.with(imgProductThumbnail.getContext()).load(product.getThumbnail()).error(R
                    .mipmap.ic_launcher).fitCenter().into(imgProductThumbnail);


        }
    }

}
