package pe.devpicon.android.buscadorliverpool.mvp.presenter;

import java.util.List;

import pe.devpicon.android.buscadorliverpool.mvp.model.Product;

/**
 * Created by armando on 4/11/17.
 */

public interface PresenterOps {
    void showResponse(String response);

    void onLoadSuccess(List<Product> products);

    void onSearchProduct(String text);

    void onFailure();
}
