package pe.devpicon.android.buscadorliverpool.data.client;

import com.google.gson.JsonObject;

import pe.devpicon.android.buscadorliverpool.data.entity.ListWrapper;
import pe.devpicon.android.buscadorliverpool.data.entity.ProductEntity;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by armando on 4/11/17.
 */

public interface LiverpoolService {

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.liverpool.com.mx/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @GET("tienda?d3106047a194921c01969dfdec083925=json")
    Call<ListWrapper<ProductEntity>> getProducts(@Query("s") String criteria);


    @GET("tienda?d3106047a194921c01969dfdec083925=json")
    Call<JsonObject> getProductsJsonObject(@Query("s") String criteria);

}
