package pe.devpicon.android.buscadorliverpool.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by armando on 4/11/17.
 */

public class DatabaseManager {
    private static final String TAG = DatabaseManager.class.getSimpleName();

    private static DatabaseManager INSTANCE;
    private CriteriaDbHelper helper;

    private DatabaseManager(Context context) {
        helper = new CriteriaDbHelper(context);
    }

    public static DatabaseManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseManager(context);
        }
        return INSTANCE;
    }


    public Cursor queryAllCriterias() {
        SQLiteDatabase database = helper.getReadableDatabase();
        return database.query(CriteriaContract.Entry.TABLE_NAME, null, null, null, null, null, null);
    }

    public void insertCriteria(String criteria) {
        SQLiteDatabase database = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CriteriaContract.Entry.COLUMN_NAME_CRITERIA, criteria);

        try {
            database.beginTransaction();
            database.insert(CriteriaContract.Entry.TABLE_NAME, null, contentValues);
            database.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e(TAG, "Ha ocurrido un error durante la inserción", e);
        } finally {
            database.endTransaction();
        }
    }

}
