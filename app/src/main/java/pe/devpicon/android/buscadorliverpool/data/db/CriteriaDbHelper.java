package pe.devpicon.android.buscadorliverpool.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by armando on 4/11/17.
 */

public class CriteriaDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "criteria.db";
    private static final int DATABASE_VERSION = 1;

    public CriteriaDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String SQL_CREATE_CRITERIA_TABLE = "CREATE TABLE " + CriteriaContract.Entry.TABLE_NAME +
                "("
                + CriteriaContract.Entry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + CriteriaContract.Entry.COLUMN_NAME_CRITERIA + " TEXT NOT NULL);";

        db.execSQL(SQL_CREATE_CRITERIA_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + CriteriaContract.Entry.TABLE_NAME);
        onCreate(db);
    }
}
