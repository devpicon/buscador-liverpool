package pe.devpicon.android.buscadorliverpool.data.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by armando on 4/11/17.
 */

public class ProductEntity {

    @SerializedName("product.displayName")
    String title;
    @SerializedName("sku.sale_Price")
    String price;
    //    @SerializedName("")
    //   String location;
    @SerializedName("product.smallImage")
    String thumbnail;

    @Override
    public String toString() {
        return "[title=" + title + "; price= " + price + "; thumbnail=" + thumbnail + "]";
    }
}
