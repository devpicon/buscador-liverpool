package pe.devpicon.android.buscadorliverpool.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by armando on 4/11/17.
 */

public class Product implements Parcelable {
    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    private String title;
    private String price;
    private String thumbnail;
    public Product() {
    }
    public Product(String title, String price, String thumbnail) {
        this.title = title;
        this.price = price;
        this.thumbnail = thumbnail;
    }

    protected Product(Parcel in) {
        this.title = in.readString();
        this.price = in.readString();
        this.thumbnail = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.price);
        dest.writeString(this.thumbnail);
    }
}
