package pe.devpicon.android.buscadorliverpool.mvp.presenter;

import java.util.List;

import pe.devpicon.android.buscadorliverpool.data.client.LiverpoolRepository;
import pe.devpicon.android.buscadorliverpool.mvp.model.Product;
import pe.devpicon.android.buscadorliverpool.mvp.view.MainView;

/**
 * Created by armando on 4/11/17.
 */

public class MainPresenter implements PresenterOps {

    private MainView view;

    public MainPresenter(MainView view) {
        this.view = view;
    }

    @Override
    public void showResponse(String response) {
        view.showResponse(response);
    }

    @Override
    public void onLoadSuccess(List<Product> products) {
        view.hideProgressBar();

        if(products != null && products.size() > 0){
            view.showRecyclerView();
            view.showProducts(products);
        } else {
            view.showEmptyListMessage();
        }
    }

    @Override
    public void onSearchProduct(String criteria) {

        view.hideErrorMessage();
        view.showProgressBar();

        LiverpoolRepository repository = new LiverpoolRepository();
        repository.getProducts(criteria, this);
        repository.saveCriteria(view.getContext(),criteria);

    }

    @Override
    public void onFailure() {
        view.hideRecyclerView();
        view.showErrorMessage();
    }
}
