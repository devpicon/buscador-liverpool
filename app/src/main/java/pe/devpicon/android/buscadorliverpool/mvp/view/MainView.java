package pe.devpicon.android.buscadorliverpool.mvp.view;

import android.content.Context;

import java.util.List;

import pe.devpicon.android.buscadorliverpool.mvp.model.Product;

/**
 * Created by armando on 4/11/17.
 */

public interface MainView {
    void showResponse(String response);

    void showProducts(List<Product> products);

    void hideProgressBar();

    void showProgressBar();

    void showRecyclerView();

    void showErrorMessage();

    void hideRecyclerView();

    void hideErrorMessage();

    void showEmptyListMessage();

    Context getContext();
}
