package pe.devpicon.android.buscadorliverpool.mvp.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import pe.devpicon.android.buscadorliverpool.R;
import pe.devpicon.android.buscadorliverpool.mvp.model.Product;
import pe.devpicon.android.buscadorliverpool.mvp.presenter.MainPresenter;
import pe.devpicon.android.buscadorliverpool.mvp.presenter.PresenterOps;

public class MainActivity extends AppCompatActivity implements MainView {

    private PresenterOps presenterOps;
    private ProductAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenterOps = new MainPresenter(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_products);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new ProductAdapter();
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void showResponse(String response) {
        TextView txtResponse = (TextView) findViewById(R.id.txt_response);
        txtResponse.setText(response);
    }

    @Override
    public void showProducts(List<Product> products) {

        adapter.setProductList(products);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void hideProgressBar() {
        ProgressBar pgbLoading = (ProgressBar) findViewById(R.id.pgb_loading);
        pgbLoading.setVisibility(View.GONE);
    }

    @Override
    public void showProgressBar() {
        ProgressBar pgbLoading = (ProgressBar) findViewById(R.id.pgb_loading);
        pgbLoading.setVisibility(View.VISIBLE);

    }

    @Override
    public void showRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_products);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorMessage() {
        TextView txtResponse = (TextView) findViewById(R.id.txt_response);
        txtResponse.setVisibility(View.VISIBLE);
        txtResponse.setText("Ocurrió un error, inténtelo de nuevo");
    }

    @Override
    public void hideRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_products);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideErrorMessage() {
        TextView txtResponse = (TextView) findViewById(R.id.txt_response);
        txtResponse.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyListMessage() {
        TextView txtResponse = (TextView) findViewById(R.id.txt_response);
        txtResponse.setVisibility(View.VISIBLE);
        txtResponse.setText("La búsqueda no trajo resultados");
    }

    @Override
    public Context getContext() {
        return this;
    }

    public void searchProduct(View view) {

        EditText edtCriteria = (EditText) findViewById(R.id.edt_criteria);
        presenterOps.onSearchProduct(edtCriteria.getText().toString());

    }
}
