package pe.devpicon.android.buscadorliverpool.data.db;

import android.provider.BaseColumns;

/**
 * Created by armando on 4/11/17.
 */

public class CriteriaContract {
    public static class Entry implements BaseColumns {

        public static final String TABLE_NAME = "criteria";
        public static final String COLUMN_NAME_CRITERIA = "criteria";


    }
}
