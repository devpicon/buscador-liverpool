package pe.devpicon.android.buscadorliverpool.data.entity;

import java.util.List;

/**
 * Created by armando on 4/11/17.
 */

public class ListWrapper<T> {
    List<T> records;

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }
}
