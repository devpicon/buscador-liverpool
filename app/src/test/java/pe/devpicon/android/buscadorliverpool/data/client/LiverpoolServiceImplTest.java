package pe.devpicon.android.buscadorliverpool.data.client;

import android.util.Log;

import org.junit.Test;

import pe.devpicon.android.buscadorliverpool.mvp.presenter.PresenterOps;

/**
 * Created by armando on 4/11/17.
 */
public class LiverpoolServiceImplTest {

    private static final String TAG = LiverpoolServiceImplTest.class.getSimpleName();

    @Test
    public void testGetProducts(){
        LiverpoolRepository service = new LiverpoolRepository();
        service.getProducts("", new PresenterOps() {
            @Override
            public void showResponse(String response) {
                Log.d(TAG, response);
                System.out.println(response);
            }
        });
    }

}